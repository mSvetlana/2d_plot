﻿///////////////////////////////////////////////////////////////////////////////
/*
 *  Класс окна для MS Windows.
 */
#pragma hdrstop
#include "h/Window.h"
///////////////////////////////////////////////////////////////////////////////
#pragma package(smart_init)

#define OFFSET 5;
#define OFFSETR 100;

Window::Window(GLWidget *view, ModelPlot *model, int width, int height, HINSTANCE hInstance, int nCmdShow) :
    cmdShow(nCmdShow)
{
    data.view = view;
    data.model = model;

    wData.hWnd   = NULL;
    wData.title  = L"KbPlot";
    wData.className = L"KbPlot_Win_Class";
    wData.x 	  = 0;
    wData.y      = 0;
    wData.width  = width;
    wData.height = height;
    wData.hInstance = hInstance;
    glData.title  = L"KbPlot";
    glData.className = L"OpenGL_Win_KbPlot";
    glData.bits  = 16;
    glData.x	  = 0;
    glData.y     = 0;
    glData.width = width   - OFFSETR;
    glData.height = height - OFFSETR;
}

Window::~Window()
{

}
///////////////////////////////////////////////////////////////////////////////
int Window::Create()//функция создания окна
{
    bool success = data.view->GLinit();

    if (!success)
    {
        MessageBox(NULL,L"GL Initialization Failed.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
        return FALSE;
    }

    bool isWinMain = CreateMainWindow();
    glData.hWndParent = wData.hWnd;
    HWND glHWND = data.view->CreateGLWindow(glData, WndProc, (LPVOID) &data);
    if (!isWinMain || !glHWND)
    {
        return FALSE;
    }

    // Задаем размер окна в соответствии с его содержимым
    RECT rect;
    ::GetWindowRect(glHWND, &rect);      // get size of glWin
    rect.right += 80 + OFFSET;	// + ширина кнопок
    ::AdjustWindowRectEx(&rect, wData.winStyle, FALSE, wData.winStyleEx);//выравнивание
    ::SetWindowPos(wData.hWnd, 0, 100, 100, rect.right-rect.left, rect.bottom-rect.top, SWP_NOZORDER);
    //-------------------------

    ::ShowWindow(wData.hWnd,  cmdShow);
    ::ShowWindow(glHWND, cmdShow);// отображение окна
    				    
    ::SetFocus(glHWND);//

    return TRUE;
}
int Window::Exec()//цикл отслеживания сообщений
{
    MSG msg;				   							// структура Windows сообщений
    BOOL  done=FALSE;                        			
    while(!done)                                		//цикл работает до тех пор,пока done=TRUE
    {
        if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))   	// ожидаем сообщение
        {
            switch(msg.message)
            {
            case WM_QUIT:
                done=TRUE;                  			
                break;
            default:
                LRESULT res;
                data.view->TryProcessMessage(msg.message, msg.wParam, msg.lParam, res);
            }
            TranslateMessage(&msg);             
            DispatchMessage(&msg);					// отправить сообщение
        }
    }
    data.view->KillGLWindow();						// убить GL-окно
    KillMainWindow();                               // убить главное окно(окно формы)
    return (msg.wParam);                            // выход из программы
}
///////////////////////////////////////////////////////////////////////////////
// главное окно(форма)
///////////////////////////////////////////////////////////////////////////////
GLvoid Window::KillMainWindow()
{
    ::UnregisterClass(wData.className, wData.hInstance);
    wData.hInstance = NULL;
}
///////////////////////////////////////////////////////////////////////////////
BOOL Window::CreateMainWindow()//создание окна формы

{
	wData.hWnd 	  = 0;
	wData.winStyle   = WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN;
	wData.winStyleEx = WS_EX_WINDOWEDGE;
	wData.x 		  = CW_USEDEFAULT;
	wData.y 		  = CW_USEDEFAULT;
	wData.width 	  = CW_USEDEFAULT;
	wData.height 	  = CW_USEDEFAULT;

	WNDCLASSEX winClass;
	   /* структура класса окна типа WNDCLASSEX.
 Эта структура содержит информацию об окне, такую
 как используемые в приложении значки, цвет фона окна,
 отображаемое в заголовке окна название, имя функции процедуры окна и т. д.
 */

	winClass.cbSize        = sizeof(WNDCLASSEX);
	winClass.style         = 0;
	winClass.lpfnWndProc   = (WNDPROC)WndProc;    		    //указатель на функцию wndProc
	winClass.cbClsExtra    = 0;
	winClass.cbWndExtra    = 0;
	winClass.hInstance     = wData.hInstance;                              	// владелец класса
	winClass.hIcon         = LoadIcon(wData.hInstance, IDI_APPLICATION);
	winClass.hIconSm       = 0;
	winClass.hCursor       = LoadCursor(0, IDC_ARROW);
	winClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	winClass.lpszMenuName  = 0;
	winClass.lpszClassName = wData.className;
	winClass.hIconSm       = LoadIcon(wData.hInstance, IDI_APPLICATION);

	// регистрация класса окна
	if(!::RegisterClassEx(&winClass))
	{
		MessageBox(NULL,L"Failed To Register The Window Class.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;
	}


    wData.hWnd = ::CreateWindowEx(
                     wData.winStyleEx,           // границы окна
                     wData.className,            // указатель на зарегистрированное имя класса
                     wData.title,                // заголовок окна
                     wData.winStyle,             // стиль окна
                     wData.x,                    //  горизонтальная позиция окна
                     wData.y,                    // вертикальная позиция окна
                     wData.width,                // ширина окна
                     wData.height,               // высота окна
                     0,         	    		// дескриптор родительского окна
                     0,           				// дескриптор меню
                     wData.hInstance,            // дескриптор экземпляра приложения
                     (LPVOID)&data);    		    // указатель на данные создания окна

    if(!wData.hWnd)
    {
        KillMainWindow();
        MessageBox(NULL,L"Window Creation Error.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
        return FALSE;
    }

    // Создание 5-и радиокнопок.
    const int posX = glData.width + OFFSET;
    const int posY = (int)OFFSET;
    CreateWindow(L"button", L"View 1",
                 WS_CHILD|WS_VISIBLE|BS_AUTORADIOBUTTON/*BS_RADIOBUTTON*/ ,
                 posX, posY, 80, 30, wData.hWnd, (HMENU)10001, wData.hInstance, NULL);
    CreateWindow(L"button", L"View 2",
                 WS_CHILD|WS_VISIBLE|BS_AUTORADIOBUTTON ,
                 posX, posY + 30, 80, 30, wData.hWnd, (HMENU)10002, wData.hInstance, NULL);
    CreateWindow(L"button", L"View 3",
                 WS_CHILD|WS_VISIBLE|BS_AUTORADIOBUTTON ,
                 posX, posY + 60, 80, 30, wData.hWnd, (HMENU)10003, wData.hInstance, NULL);
    CreateWindow(L"button", L"View 4",
                 WS_CHILD|WS_VISIBLE|BS_AUTORADIOBUTTON,
                 posX, posY + 90, 80, 30, wData.hWnd, (HMENU)10004, wData.hInstance, NULL);
    CreateWindow(L"button", L"View 5",
                 WS_CHILD|WS_VISIBLE|BS_AUTORADIOBUTTON,
                 posX, posY + 120, 80, 30, wData.hWnd, (HMENU)10005, wData.hInstance, NULL);

    //  Создание кнопки сохранения рисунка

    CreateWindow(
        L"BUTTON", /* создаем кнопку */
        L"Save Graph", /* текст, появившейся на кнопке */
        WS_VISIBLE | WS_CHILD,
        posX, /*размер кнопки*/
        posY + 150,
        80,
        30,
        wData.hWnd, /* кнопки родительского(главного) окна */
        (HMENU)10006, /* действи я,при нажании на кнопки*/
        wData.hInstance,
        NULL);

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
/* функция обрабатывает сообщения, которые приложение получает от операционной системы. 
Например, при нажатии на кнопки пользователем система отправляет в приложение сообщение, 
что эта кнопка была нажата. Функция WndProc отвечает за реагирование на это событие.*/
LRESULT CALLBACK Window::WndProc(   HWND    hWnd,// дескриптор окна
                                    UINT    uMsg, //сообщение окну
                                    WPARAM  wParam,  //дополнительные параметры
                                    LPARAM  lParam) //       
{

    // Так как ф-ция статична, нужно получить необходимые данные (указатели на GLWidget и ModelPlot) из параметров ф-ции
    ////////////////////
    static MVData *data;
    data = (MVData*)::GetWindowLongPtr(hWnd, GWL_USERDATA);

    // Получаем структуру MVData* c указателями на GLWidget и ModelPlot
    if(uMsg == WM_NCCREATE)  // системное сообщение
    {
      
        data = (MVData*)(((CREATESTRUCT*)lParam)->lpCreateParams);
        if(data)
        {
  
            ::SetWindowLongPtr(hWnd, GWL_USERDATA, (LONG_PTR)data);
        }
        else
            MessageBox(NULL,L"[ERROR] Failed to create controller::windowProcedure().",L"",MB_OK|MB_ICONQUESTION);

        return ::DefWindowProc(hWnd, uMsg, wParam, lParam);
    }
 
    if(!data)
        return ::DefWindowProc(hWnd, uMsg, wParam, lParam);

    switch (uMsg)                               
    {
    case WM_COMMAND:
    {
        // radiobuttons
        if(LOWORD(wParam) >= 10001 && LOWORD(wParam) <= 10005)
        {
            // очистка контейнеров в glWidget
            data->view->clearContainers();
            // Если мы нажали на 1-й радиокнопке.
            if(LOWORD(wParam)==10001)
                data->model->CreateOption1();
            // Если мы нажали на 2-й радиокнопке.
            if(LOWORD(wParam)==10002)
                data->model->CreateOption2();
            // Если мы нажали на 3-й радиокнопке.
            if(LOWORD(wParam)==10003)
                data->model->CreateOption3();
			// Если мы нажали на 4-й радиокнопке.	
            if(LOWORD(wParam)==10004)
                data->model->CreateOption4();
			// Если мы нажали на 5-й радиокнопке.
            if(LOWORD(wParam)==10005)
                data->model->CreateOption5();
            data->view->GLupdate();
        }
        else if(LOWORD(wParam)==10006) //нажатие на копку
        {
            OPENFILENAME ofn;
            wchar_t szFileName[MAX_PATH] = L" ";
            ZeroMemory (&ofn, sizeof(ofn));
            ofn.lStructSize = sizeof(ofn);
            ofn.hwndOwner = hWnd;
            ofn.lpstrFilter = L"Png Files (*.png)\0*.png\0";
            ofn.lpstrFile = szFileName;
            ofn.nMaxFile = MAX_PATH;
            ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST;
            ofn.lpstrDefExt = L"png";
            if(GetSaveFileName(&ofn))
            {
                data->view->exportAsImage(szFileName);
            }
        }
        return 0;
    }

    default:
    {
        LRESULT res;
        if(data->view->TryProcessMessage(uMsg, wParam, lParam, res))
            return res;
    }
    }
    
    return DefWindowProc(hWnd,uMsg,wParam,lParam);
}
///////////////////////////////////////////////////////////////////////////////
// загрузка иконки
///////////////////////////////////////////////////////////////////////////////
HICON Window::loadIcon(HINSTANCE hInstance,int id)
{
    return (HICON)::LoadImage(hInstance, MAKEINTRESOURCE(id), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE);
}
///////////////////////////////////////////////////////////////////////////////
//загрузка курсора
///////////////////////////////////////////////////////////////////////////////
HICON Window::loadCursor(HINSTANCE hInstance,int id)
{
    return (HCURSOR)::LoadImage(hInstance, MAKEINTRESOURCE(id), IMAGE_CURSOR, 0, 0, LR_DEFAULTSIZE);
}

﻿#include "BitmapFont.h"


BitmapFont::BitmapFont(HDC hDC) : hDC_(hDC)
{
}


BitmapFont::~BitmapFont(void)
{
	KillFont();
}

// настройка шрифта и указание списка символов для вывода текста с помощью win api
GLvoid BitmapFont::BuildFont(size_t size)								// Build Our Bitmap Font
{
	HFONT	font;										// Windows Font ID
	HFONT	oldfont;									// Used For Good House Keeping

	base_ = glGenLists(256);								// список отображения 96 символов

	font = CreateFont(	-(int)size,							// высота шрифта
						0,								// средяя ширина символа
						0,								// Устанавливает угол, в десятых градуса, между вектором наклона и осью X устройства. Вектор наклона параллелен основной линии ряда текста.
						0,								// угол ориентации базисной линии
						FW_REGULAR,						// толщина шрифта
						FALSE,							// описатель параметра курсивного шрифта
						FALSE,							// описатель параметра подчеркивания
						FALSE,							// описатель параметра зачеркивания
						RUSSIAN_CHARSET,				// идентификатор набора символов
						OUT_TT_PRECIS,					// точность вывода
						CLIP_DEFAULT_PRECIS,			// точность отсечения
						ANTIALIASED_QUALITY,			// качество вывода
						FF_DONTCARE|DEFAULT_PITCH,		// шаг между символами шрифта и семейство
						L"Courier New");				// азвание шрифта

	oldfont = (HFONT)SelectObject(hDC_, font);           // устанавливаем нужный нам шрифт
	wglUseFontBitmapsA(hDC_, 0, 256, base_);			 // Создает набор изображений из выбранного в данный момент GDI шрифта.
	SelectObject(hDC_, oldfont);						// SelectObject выбирает объект для использования его в устройстве.
														//Объекяаженит имеет растр,кисть,шрифт,перо,регион и т.д.
														//Выбранный объект может использоваться устройством,сколько это может вам понадобиться.Функция возвращает дескриптор.
	DeleteObject(font);									// удаляем объект шрифта
}

GLvoid BitmapFont::KillFont(GLvoid)						// удаляем шрифт
{
	glDeleteLists(base_, 256);							// удаляем список отобр
}

GLvoid BitmapFont::Print(GLfloat x, GLfloat y, const char *fmt, ...)					// печать текста
{
	glRasterPos2f(x, y);

	char		text[256];
	va_list		ap;										//указатель на список аргументов

	if (fmt == NULL)									// если нет текста, ничего не делай
		return;											//

	va_start(ap, fmt);
	vsprintf(text, fmt, ap);
	va_end(ap);

	glPushAttrib(GL_LIST_BIT);
	glListBase(base_);
	glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);
	glPopAttrib();
}


﻿//---------------------------------------------------------------------------

#ifndef GLWIDGET_H
#define GLWIDGET_H

//---------------------------------------------------------------------------
#include <map>
#include <string>
#include <vector>

#include "event.h"
#include "aglwidget.h"
#include "graphical_object.h"

using std::string;
using std::vector;
using std::map;

struct GLWindowData
{
    /*
     * структура, содержащая необходимые для
     * создания openGL окна поля
     */
	HWND      hWndParent;     // указатель на окно родителя( MainWindowData)
	LPWSTR 	  title;
	LPWSTR 	  className;
	int x;                    // позиция окна по  X
	int y;                    // позиция окна по Y
	int width;
	int height;
	int bits; //число бит на ,,,,
};

struct InternalGLWindowData
{
	LPWSTR 	  className;      // название класса
	HGLRC     hRC;		      // порт для связи OpenGL и системы
	HDC	      hDC;		      // связь с монитором
	HWND      hWnd;		      // хэгдэл окна
	HINSTANCE hInstance;	  // содержит экземпляр объекта  GL window
	GLWindowData data;
};


class GLWidget : public AGLWidget
{
	/*
	 * Класс, представляющий собой конкретную реализацию
	 * интерфейса AGLWidget для ОС Windows и среды Borland Builder
	 */
public:
	GLWidget();
	virtual ~GLWidget();

	///////////////////////////////////////////////////////////////
	// GL Window
	HWND CreateGLWindow(const GLWindowData wndData, WNDPROC WndProc, LPVOID WndProcData);
	void KillGLWindow();
	// Обработка WINAPI-сообщения
	bool TryProcessMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT& res);

private:
	// gl funcs	 /* ex Qt part */
	bool initializeGL();
	void resizeGL(int nw, int nh);
	void paintGL();
public:
	//Adapter part
	virtual void GLpaint();
	virtual void GLresize(int nw, int nh);
	virtual void GLupdate();
	virtual bool GLinit();
	virtual void mousePressedEvent(int xPos, int yPos);
	virtual void mouseMoveEvent(int xPos, int yPos);
	virtual void mouseReleaseEvent(int xPos, int yPos);
	virtual void wheelEvent(int delta);
	virtual void subscribeToMouse(IMouseEventListener*);

	virtual double trScreenToGLx(int);
	virtual double trScreenToGLy(int);
	virtual int trGLToScreenx(double);
	virtual int trGLToScreeny(double);


	virtual void addLayer(const string &key);
	virtual void addObject(const string &key, GraphicalObject* obj);

	virtual void removeLayer(const string &key);

	virtual void clearLayer(const string &key);
	virtual void clearScene();

	// очистка контейнеров
	// используется в примере,
	// вызывается перед сменой примера
	// и перед удалением окна
	virtual void clearContainers();

	virtual void setWorkingArea(double xmin, double xmax, double ymin, double ymax);

	virtual void exportAsImage(const wchar_t* filename);

public:
	int width();
	int height();
private:
	int mouseX, mouseY;
	map<string, vector<GraphicalObject*> >objects;
	vector<IMouseEventListener*> mouseListeners;
	int w;
	int h;

	static bool active;				// Флаг активного окна,по умолчанию TRUE
	InternalGLWindowData itlWndData;
};

#endif // GLWIDGET_H


﻿//---------------------------------------------------------------------------
#ifndef MODELPLOT_H
#define MODELPLOT_H
//---------------------------------------------------------------------------
#include "KbPlot.h"
#include "GLWidget.h"
 /*
  * используя модель MVC,данный класс является компонентом модель.
  5 опций демонстрируют 5 различных вариантов использования данной библиотеки.
  Так Вы можете увидеть 5 примеров графиков, с различными заданными параметрами:цвет,толщина линий,текст,стиль и тд.
  */
class ModelPlot
{
public:
    ModelPlot(GLWidget* view);
    ~ModelPlot();

    void Clear();
public: //построение графиков,с различными заданными параметрами:цвет,толщина линий,текст,стиль и тд
	void CreateOption1();//рисует график с линиями зеленого цвета,толщиной 1,точками размера 20 в виде треугольников,с подписями вершин под вершинами
	void CreateOption2();
	void CreateOption3();
	void CreateOption4();
	void CreateOption5();
private:

	KbPlot *plot;
	unsigned char currentOption;
	GLWidget *gl;
	DataSet *dataset;
	Style *style;
	std::map<size_t,string>  marks;
	std::vector<Txy> points;
	double rawdata[256];
	RawData<double> *rd;

};
#endif//MODELPLOT_H

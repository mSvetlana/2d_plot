#pragma once

#ifndef BITMAP_FONT_H
#define BITMAP_FONT_H
#include <stdio.h>
#include <stdarg.h>
#include "gl_helper.h"

class BitmapFont
{
public:
	BitmapFont(HDC hDC = NULL);
	~BitmapFont(void);

	GLvoid BuildFont(size_t);				//������� �����
	GLvoid KillFont(GLvoid);				// ������� �����
	GLvoid Print(GLfloat x, GLfloat y, const char *fmt, ...);

private:
	// FONT
	GLuint	base_;				//��� ��������� ������ ��������, ������������ ��� ������ ������ � ������� win api
	HDC hDC_;

};
#endif//BITMAP_FONT_H
